const express = require("express");
const server = require("./src/app");

server(express()).listen(8001, ()=> {
    console.log("App is running at http://localhost:8001");
});