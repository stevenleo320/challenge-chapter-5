# RESTful API Authentication

Berikut tujuan dari tugas `Challenge Chapter 5` :
1. Pindahkan code challenge pada chapter 3 dan 4
yang semula merupakan HTML statis ke dalam
server menggunakan Express, halaman 1
dengan yang lainnya dibedakan dengan routing
2. Membuat data user static untuk login di bagian
backend
3. Menggunakan Postman untuk mengecek API
4. Serving data user static ke bentuk JSON

Endpoint ini menggunakan prefix `/`.

Mari kita breakdown fitur - fitur yang dimiliki oleh sistem ini.

- `GET` /home 

- `GET` /game

- `POST` /auth/login

Payload

```json
{
  "username": "email@address.com",
  "password": "secretPassword"
}
```

Response

```json
{
  "accessToken": "secretToken",
  "accessTokenExpired": "2022/02/01T22:30:10.000Z",
  "refreshToken": "refreshToken",
  "refreshTokenExpired": "2022/02/01T22:30:10.000Z"
}
```

- `GET` /me

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
{
  ...userData
}
```

- `GET` /users

Request Headers

```json
{
  "Authorization": "{{accessToken}}"
}
```

Response

```json
[
  { ...userData },
  { ...userData }
]
```

## Credentials

Username: `porsche.huel@email.com`
Password: `password123`

Username: `julius.oconner@email.com`
Password: `password123456`

Username: `melodee.dooley@email.com`
Password: `password123456789`
