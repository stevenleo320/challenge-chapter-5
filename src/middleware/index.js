const jwt = require("jsonwebtoken");
const { addHours, isAfter } = require("date-fns");

// Protected Endpoint
/**
 * Access token checker flow
 * - Get Access token from request headers
 * - Verifikasi access token menggunakan module jsonwebtoken
 * - Is token still valid
 * - Valid ? go next
 */

module.exports = {
    isAuthenticated: (req,res,next) => {
        // untuk mengambil informasi header dari request nya
        console.log(req.headers,'ini header');
        console.log(req.headers.authorization,'ini auto');
        const accessToken = req.headers.authorization;
        if(!accessToken) {
            return res.status(404).json({ error : "Page not found"});
        }
        
        try {
            const isVerified = jwt.verify(accessToken, process.env.JWT_SECRET);

            if(!isVerified) {
                return res.status(404).json({ error : "Page not found"});
            }
    
            req.session = jwt.decode(accessToken);
            
            // Missing code : check expired token 
            const currentHour = Date.now();
            const accessTokenExpiredAt = addHours(req.session.loggedInAt, 1); 

            if(isAfter(currentHour, accessTokenExpiredAt)) {
                return res.status(404).json({ error : "Access token has expired"});
            }
            next();
        } catch(error) {
            return res.status(404).json({ error : "Page not found"});
        }
        
    },
};