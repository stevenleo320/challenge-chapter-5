const express = require("express");
const bcrypt = require("bcrypt");
const { addHours, addMinutes } = require("date-fns");

const db = require("./db/users.json");
const { isAuthenticated } = require("./middleware");
const { generateToken } = require("./utils/common");
const route = express.Router();

// Routing
route.get("/home", (req, res) => {
    // ambil data dari form todo ke todos
    res.render("home");
});

// Bila di autheticated tidak bisa langsung dilihat lewat browser karena perlu header authorization
route.get("/game" ,(req, res) => {
    req.log.info("Start playing game");

    res.render("game");
});

// Route tampilan login apabila diperlukan
route.get("/login", (req, res) => {
    res.render("login",{
        status : 0
    });
});

/**
 * Login Flow
 * - Make sure username and password are not empty
 * - Check username from database
 * - Compare password
 * - Generate token
 * - Return token
 */
route.post("/auth/login", (req, res, next) => { 
    const { username, password } = req.body;

    // Make sure username & password are filled
    if (!username && !password) {
        return res.status(412).json({ error: "Missing fields! Username and password are required." });
        // Kalo mau menampilkan error di login.jes
        // res.render("login",{
        //     status : 412
        // });
    }

    // Check username from database
    const user = db.find((user) => user.email === username);
    if (!user) {
        return res.status(404).json({ error: "User not found!" });
        // Kalo mau menampilkan error di login.jes
        // res.render("login",{
        //     status : 404
        // });
    }

    // Compare password
    const isValid = bcrypt.compareSync(password, user.password);
    if (!isValid) {
        return res.status(404).json({ error: "User not found isValid!" });
        // Kalo mau menampilkan error di login.jes
        // res.render("login",{
        //     status : 404
        // });
    }

    req.user = user;

    next();
  },
  (req, res) => {
    const { username, email } = req.user; 

    const loggedInAt = Date.now();
    const accessToken = generateToken({loggedInAt,user: { username, email },});
    const accessTokenExpiredAt = addHours(loggedInAt, 1);
    const refreshToken = generateToken({loggedInAt,});
    const refreshTokenExpiredAt = addMinutes(loggedInAt, 50);

    res.json({
      accessToken,
      accessTokenExpiredAt,
      refreshToken,
      refreshTokenExpiredAt,
    });
  }
);

// Cek data user yang sedang login
route.get("/me", isAuthenticated, (req, res) => {
  req.log.info("Start getting user information");
  const user = db.find((user) => user.username === req.session.user.username);
  res.json(user);
});

// Cek data user
route.get("/users", isAuthenticated, (req, res) => {
  res.json(db);
});

module.exports = route;

